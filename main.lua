local mod = RegisterMod("Aintramural", 1)
local game = Game()
local ModRNG = RNG()
local sound = SFXManager()
mod.Aintramural = Isaac.GetItemIdByName("Aintramural")

if not __eidItemDescriptions then __eidItemDescriptions = {} end
__eidItemDescriptions[mod.Aintramural] = "Gives a 1/6 chance to create a duplicate any hostile or special room, on first visit."

local RoomTypesByString = {
	[RoomType.ROOM_DEFAULT] = "d.",
	[RoomType.ROOM_SHOP] = "s.shop.",
	[RoomType.ROOM_ERROR] = "s.error.",
	[RoomType.ROOM_TREASURE] = "s.treasure.",
	[RoomType.ROOM_BOSS] = "s.boss.",
	[RoomType.ROOM_MINIBOSS] = "s.miniboss.",
	[RoomType.ROOM_SECRET] = "s.secret.",
	[RoomType.ROOM_SUPERSECRET] = "s.supersecret.",
	[RoomType.ROOM_ARCADE] = "s.arcade.",
	[RoomType.ROOM_CURSE] = "s.curse.",
	[RoomType.ROOM_CHALLENGE] = "s.challenge.",
	[RoomType.ROOM_LIBRARY] = "s.library.",
	[RoomType.ROOM_SACRIFICE] = "s.sacrifice.",
	[RoomType.ROOM_DEVIL] = "s.devil.",
	[RoomType.ROOM_ANGEL] = "s.angel.",
	[RoomType.ROOM_DUNGEON] = "s.itemdungeon.",
	[RoomType.ROOM_BOSSRUSH] = "s.bossrush.",
	[RoomType.ROOM_ISAACS] = "s.isaacs.",
	[RoomType.ROOM_BARREN] = "s.barren.",
	[RoomType.ROOM_CHEST] = "s.chest.",
	[RoomType.ROOM_DICE] = "s.dice.",
	[RoomType.ROOM_BLACK_MARKET] = "s.blackmarket.",
	[RoomType.ROOM_GREED_EXIT] = "d.",
}

local TotalRoomsByTypeDefault = {
	[LevelStage.STAGE1_1] = {
		[StageType.STAGETYPE_ORIGINAL] = 1083,
		[StageType.STAGETYPE_WOTL] = 1053,
		[StageType.STAGETYPE_AFTERBIRTH] = 1160,
	},
	[LevelStage.STAGE1_2] = {
		[StageType.STAGETYPE_ORIGINAL] = 1083,
		[StageType.STAGETYPE_WOTL] = 1053,
		[StageType.STAGETYPE_AFTERBIRTH] = 1160,
	},
	[LevelStage.STAGE2_1] = {
		[StageType.STAGETYPE_ORIGINAL] = 951,
		[StageType.STAGETYPE_WOTL] = 953,
		[StageType.STAGETYPE_AFTERBIRTH] = 1081,
	},
	[LevelStage.STAGE2_2] = {
		[StageType.STAGETYPE_ORIGINAL] = 951,
		[StageType.STAGETYPE_WOTL] = 953,
		[StageType.STAGETYPE_AFTERBIRTH] = 1081,
	},
	[LevelStage.STAGE3_1] = {
		[StageType.STAGETYPE_ORIGINAL] = 975,
		[StageType.STAGETYPE_WOTL] = 1000,
		[StageType.STAGETYPE_AFTERBIRTH] = 1051,
	},
	[LevelStage.STAGE3_2] = {
		[StageType.STAGETYPE_ORIGINAL] = 975,
		[StageType.STAGETYPE_WOTL] = 1000,
		[StageType.STAGETYPE_AFTERBIRTH] = 1051,
	},
	[LevelStage.STAGE4_1] = {
		[StageType.STAGETYPE_ORIGINAL] = 916,
		[StageType.STAGETYPE_WOTL] = 916,
		[StageType.STAGETYPE_AFTERBIRTH] = 985,
	},
	[LevelStage.STAGE4_2] = {
		[StageType.STAGETYPE_ORIGINAL] = 916,
		[StageType.STAGETYPE_WOTL] = 916,
		[StageType.STAGETYPE_AFTERBIRTH] = 985,
	},
	[LevelStage.STAGE4_3] = {
		[StageType.STAGETYPE_ORIGINAL] = 1,
		[StageType.STAGETYPE_WOTL] = 1,
		[StageType.STAGETYPE_AFTERBIRTH] = 1,
	},
	[LevelStage.STAGE5] = {
		[StageType.STAGETYPE_ORIGINAL] = 456,
		[StageType.STAGETYPE_WOTL] = 360,
		[StageType.STAGETYPE_AFTERBIRTH] = 1,
	},
	[LevelStage.STAGE6] = {
		[StageType.STAGETYPE_ORIGINAL] = 326,
		[StageType.STAGETYPE_WOTL] = 323,
		[StageType.STAGETYPE_AFTERBIRTH] = 1,
	},
	[LevelStage.STAGE7] = {
		[StageType.STAGETYPE_ORIGINAL] = 1083,
		[StageType.STAGETYPE_WOTL] = 1083,
		[StageType.STAGETYPE_AFTERBIRTH] = 1,
	},
}

local TotalRoomsByTypeSpecial = {
	[RoomType.ROOM_SHOP] = 6,
	[RoomType.ROOM_ERROR] = 27,
	[RoomType.ROOM_TREASURE] = 38,
	[RoomType.ROOM_BOSS] = 1,
	[RoomType.ROOM_MINIBOSS] = 1,
	[RoomType.ROOM_SECRET] = 29,
	[RoomType.ROOM_SUPERSECRET] = 19,
	[RoomType.ROOM_ARCADE] = 20,
	[RoomType.ROOM_CURSE] = 24,
	[RoomType.ROOM_CHALLENGE] = 12,
	[RoomType.ROOM_LIBRARY] = 15,
	[RoomType.ROOM_SACRIFICE] = 9,
	[RoomType.ROOM_DEVIL] = 18,
	[RoomType.ROOM_ANGEL] = 15,
	[RoomType.ROOM_DUNGEON] = 10,
	[RoomType.ROOM_BOSSRUSH] = 5,
	[RoomType.ROOM_ISAACS] = 27,
	[RoomType.ROOM_BARREN] = 25,
	[RoomType.ROOM_CHEST] = 41,
	[RoomType.ROOM_DICE] = 20,
	[RoomType.ROOM_BLACK_MARKET] = 8, -- Actually 9
	
	BossIndexes = {
		1000, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023,
		1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038,
		1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053,
		1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068,
		1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083,
		1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098,
		1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113,
		1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128,
		1129, 1130, 1131, 1533, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2020, 2021,
		2022, 2023, 2024, 2025, 2030, 2031, 2032, 2033, 2035, 2036, 2040, 2041, 2042, 2043, 2050,
		2051, 2052, 2053, 2060, 2061, 2062, 2063, 2064, 2065, 2070, 2071, 2072, 2073, 3260, 3261,
		3262, 3263, 3264, 3265, 3266, 3267, 3268, 3270, 3271, 3272, 3273, 3280, 3281, 3282, 3283,
		3290, 3291, 3292, 3293, 3294, 3295, 3300, 3301, 3302, 3303, 3310, 3311, 3312, 3313, 3320,
		3321, 3322, 3323, 3330, 3331, 3332, 3333, 3340, 3341, 3342, 3343, 3344, 3345, 3350, 3351,
		3352, 3353, 3360, 3361, 3362, 3363, 3370, 3371, 3372, 3373, 3374, 3375, 3376, 3377, 3378,
		3379, 3380, 3381, 3382, 3383, 3384, 3385, 3386, 3387, 3388, 3389, 3390, 3391, 3392, 3393,
		3394, 3395, 3396, 3397, 3398, 3399, 3400, 3401, 3402, 3403, 3404, 3405, 3406, 3407, 3408,
		3409, 3410, 3411, 3412, 3413, 3414, 3500, 3501, 3502, 3600, 3700, 3701, 3702, 3703, 3704,
		3705, 3706, 3707, 3708, 3709, 3710, 3711, 3712, 3713, 3714, 3715, 3716, 3717, 3718, 3719,
		3720, 3721, 3722, 3723, 3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759, 3760, 3761,
		3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769, 3770, 3771, 3772, 3773, 3774, 3775, 3776,
		3777, 3778, 3800, 3801, 3802, 3804, 3805, 3806, 3807, 3809, 3810, 3811, 3812, 3813, 3814,
		3815, 3816, 3817, 3818, 3819, 4010, 4011, 4012, 4013, 4014, 4020, 4021, 4022, 4023, 4030,
		4031, 4032, 4033, 4034, 4035, 4036, 4040, 4041, 4042, 4043, 4050, 4051, 4052, 5000, 5010,
		5011, 5012, 5013, 5014, 5020, 5021, 5022, 5023, 5024, 5025, 5026, 5030, 5031, 5032, 5033,
		5034, 5035, 5040, 5041, 5042, 5043, 5044, 5045, 5050, 5051, 5052, 5053, 5054, 5060, 5061,
		5062, 5063, 5064, 5070, 5071, 5072, 5080, 5081, 5082, 5083, 5084, 5090, 5091, 5092, 5093,
		5094, 5100, 5101, 5102, 5103, 5104, 5105, 5106, 5110, 5111, 5113, 5130, 5140, 5141, 5142,
		5143, 5144, 5145, 5146, 5147, 5148, 5149, 5150, 5151, 5152, 5153, 5154, 5155,
	},
	MinibossIndexes = {
		2000, 2001, 2002, 2003, 2004, 2005, 2010, 2011, 2012, 2013, 2014, 2015, 2020,
		2021, 2022, 2023, 2024, 2025, 2030, 2031, 2032, 2033, 2034, 2035, 2040, 2041,
		2042, 2043, 2044, 2045, 2050, 2051, 2052, 2053, 2054, 2055, 2050, 2061, 2062,
		2063, 2064, 2065, 2100, 2101, 2102, 2103, 2104, 2110, 2111, 2112, 2113, 2114,
		2120, 2121, 2122, 2123, 2124, 2130, 2131, 2132, 2133, 2134, 2140, 2141, 2142,
		2143, 2144, 2150, 2151, 2152, 2153, 2154, 2160, 2161, 2162, 2163, 2164, 2260,
		2261, 2262, 2300, 2301, 2302, 2303, 2304, 2305, 2306
	},
}

local BossToStageTable = {
	-- Chapter 1
	[EntityType.ENTITY_MONSTRO] = 2,
	[EntityType.ENTITY_DINGLE] = 2,
	[EntityType.ENTITY_GEMINI] = 2,
	[EntityType.ENTITY_GURGLING] = 2,
	[EntityType.ENTITY_FAMINE] = 2,
	[EntityType.ENTITY_LITTLE_HORN] = 2,
	[EntityType.ENTITY_RAG_MAN] = 2,
	[EntityType.ENTITY_THE_HAUNT] = 2,
	[EntityType.ENTITY_PIN] = 3,
	[EntityType.ENTITY_DUKE] = 3,
	[EntityType.ENTITY_LARRYJR] = 3,
	[EntityType.ENTITY_WIDOW] = 3,
	[EntityType.ENTITY_FISTULA_BIG] = 4,
	[EntityType.ENTITY_FISTULA_MEDIUM] = 4,
	[EntityType.ENTITY_FISTULA_SMALL] = 4,
	
	-- Chapter 2
	[EntityType.ENTITY_CHUB] = 4,
	[EntityType.ENTITY_DARK_ONE] = 4,
	[EntityType.ENTITY_GURDY] = 4,
	[EntityType.ENTITY_PESTILENCE] = 4,
	[EntityType.ENTITY_GURDY_JR] = 4,
	[EntityType.ENTITY_MEGA_FATTY] = 4,
	[EntityType.ENTITY_MEGA_MAW] = 4,
	[EntityType.ENTITY_FORSAKEN] = 4,
	[EntityType.ENTITY_BIG_HORN] = 4,
	[EntityType.ENTITY_RAG_MEGA] = 4,
	[EntityType.ENTITY_POLYCEPHALUS] = 4,
	[EntityType.ENTITY_PEEP] = 5,
	
	-- Chapter 3
	[EntityType.ENTITY_ADVERSARY] = 6,
	[EntityType.ENTITY_CAGE] = 6,
	[EntityType.ENTITY_GATE] = 6,
	[EntityType.ENTITY_MONSTRO2] = 6,
	[EntityType.ENTITY_BROWNIE] = 6,
	[EntityType.ENTITY_MASK_OF_INFAMY] = 6,
	[EntityType.ENTITY_HEART_OF_INFAMY] = 6,
	[EntityType.ENTITY_MOM] = 6,
	[EntityType.ENTITY_LOKI] = 7,
	[EntityType.ENTITY_WAR] = 7,
	[EntityType.ENTITY_SISTERS_VIS] = 7,
	
	-- Chapter 4
	[EntityType.ENTITY_BLASTOCYST_BIG] = 8,
	[EntityType.ENTITY_BLASTOCYST_MEDIUM] = 8,
	[EntityType.ENTITY_BLASTOCYST_SMALL] = 8,
	[EntityType.ENTITY_EMBRYO] = 8,
	[EntityType.ENTITY_DADDYLONGLEGS] = 8,
	[EntityType.ENTITY_MAMA_GURDY] = 8,
	[EntityType.ENTITY_DEATH] = 8,
	[EntityType.ENTITY_MR_FRED] = 8,
	[EntityType.ENTITY_MATRIARCH] = 8,
	[EntityType.ENTITY_MOMS_HEART] = 8,
	[EntityType.ENTITY_HUSH] = 9,
	
	-- Chapter 5
	[EntityType.ENTITY_ISAAC] = 10,
	[EntityType.ENTITY_SATAN] = 10,
	
	-- Chapter 6
	[EntityType.ENTITY_MATRIARCH] = 11,
	[EntityType.ENTITY_THE_LAMB] = 11,
	[EntityType.ENTITY_MEGA_SATAN] = 11,
	[EntityType.ENTITY_MEGA_SATAN_2] = 11,
	
	-- Chapter 7
	[EntityType.ENTITY_DELIRIUM] = 12,
}

local NewRoomVisited = 84
local LastRoomVisited = 84
local TeleportOnUpdate

--------= MOD sETUP =--------
----= Seed the RNG for the run.
mod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function()
	ModRNG:SetSeed(game:GetSeeds():GetStartSeed(), 35)
	NewRoomVisited = 84
	LastRoomVisited = 84
	TeleportOnUpdate = nil
end)

----= Manage duplicate rooms.
mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	local room = game:GetRoom()
	if room:GetFrameCount() > 1 then return end
	
	local ProcFunction = false
	local player = Isaac.GetPlayer(0)

	for p = 0, game:GetNumPlayers() - 1 do
		player = Isaac.GetPlayer(p)
		
		if player:HasCollectible(mod.Aintramural) then
			ProcFunction = true
			break
		end
	end
	
	if ProcFunction == false then return end
	
	----= If the item procced on a boss room.
	if TeleportOnUpdate then
		Isaac.ExecuteCommand("goto s.boss." .. tostring(TeleportOnUpdate))
		Isaac.GetPlayer(0):UseActiveItem(CollectibleType.COLLECTIBLE_DELIRIOUS, false, false, true ,false)
		
		TeleportOnUpdate = nil
		room:Update()
	end
	
	local level = game:GetLevel()
	local curRoom = level:GetCurrentRoomIndex()
	local roomType = room:GetType()
	
	-- If it's a secret room, blow the exits.
	if curRoom == -3 and (roomType == RoomType.ROOM_SECRET or roomType == RoomType.ROOM_SUPERSECRET) then
		for i = 0, 7 do
			local door = room:GetDoor(i)
			
			if door then
				door:TryBlowOpen(true)
			end
		end
	end
	
	-- Manage deli friends and new bosses.
	for _, ent in pairs(Isaac.FindInRadius(player.Position, 1500, EntityPartition.ENEMY)) do
		if ent:HasEntityFlags(EntityFlag.FLAG_FRIENDLY) and ent.FrameCount < 2 then
			ent:Remove()
		elseif ent:IsBoss() and room:GetType() == RoomType.ROOM_BOSS and curRoom == -3 then
			local OgBossStage = BossToStageTable[ent.Type]
			
			if OgBossStage and not ent:GetData().AintramuralHPProc then
				local NewHPVal = level:GetStage() / OgBossStage
				
				ent.MaxHitPoints = ent.MaxHitPoints * NewHPVal
				ent.HitPoints = ent.MaxHitPoints
				ent:GetData().AintramuralHPProc = true
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	----= Check if a player has Aintramural.
	local ProcFunction = false
	local player = Isaac.GetPlayer(0)
	
	for p = 0, game:GetNumPlayers() - 1 do
		player = Isaac.GetPlayer(p)
		
		if player:HasCollectible(mod.Aintramural) then
			ProcFunction = true
			break
		end
	end
	
	if ProcFunction == false then return end

	----= Check if the gamemode is not greed, and other conditions.
	local room = game:GetRoom()
	local level = game:GetLevel()
	local curRoom = level:GetCurrentRoomIndex()
	
	LastRoomVisited = (not LastRoomVisited or LastRoomVisited == -3) and 84 or LastRoomVisited
	
	if NewRoomVisited == -3 then
		NewRoomVisited = curRoom
		level:ChangeRoom(level:GetRoomByIdx(LastRoomVisited).SafeGridIndex)
		return
	end
	
	-- If the room is out of bounds.
	if curRoom == -3 then
		NewRoomVisited = curRoom
		return
	else
		LastRoomVisited = LastRoomVisited == NewRoomVisited and LastRoomVisited or NewRoomVisited 
		NewRoomVisited = curRoom
	end 
	
	if game.Difficulty > 1 or room:IsFirstVisit() == false or curRoom == 84 then -- 84 is the starting room index.
		return
	end
	
	----= Run the item code.
	local roomType = room:GetType()
	local rollChance
	local roomRNG
	
	if roomType == RoomType.ROOM_DEFAULT then
		rollChance = 0.17
		roomRNG = ModRNG:RandomInt(TotalRoomsByTypeDefault[level:GetStage()][level:GetStageType()]) + 1
	else
		rollChance = 0.17
		roomRNG = ModRNG:RandomInt(TotalRoomsByTypeSpecial[roomType]) + 1
		
		roomRNG = roomType == RoomType.ROOM_MINIBOSS and TotalRoomsByTypeSpecial.MinibossIndexes[ModRNG:RandomInt(#TotalRoomsByTypeSpecial.MinibossIndexes) + 1] or roomRNG
		roomRNG = roomType == RoomType.ROOM_BLACK_MARKET and roomRNG + 1 or roomRNG
	end
	
	if ModRNG:RandomFloat() <= rollChance then
		if roomType == RoomType.ROOM_BOSS then
			TeleportOnUpdate = TotalRoomsByTypeSpecial.BossIndexes[ModRNG:RandomInt(#TotalRoomsByTypeSpecial.BossIndexes) + 1]
			return
		end
		
		Isaac.ExecuteCommand("goto " .. tostring(RoomTypesByString[roomType]) .. tostring(roomRNG))
		Isaac.GetPlayer(0):UseActiveItem(CollectibleType.COLLECTIBLE_DELIRIOUS, false, false, true ,false)

		room:Update()
	end
end)

--------------------------------==--------------------------------
--------------------------------==--------------------------------
Isaac.DebugString("Aintramural: Loaded Successfully!")
--------------------------------==--------------------------------
--------------------------------==--------------------------------